package io.github.wx.common.constant;

/**
 * Created by jie on 2017/4/27.
 * 业务公告常量
 */
public class ConBizConstant {
    /**
     * 日志的队列名
     */
    public static final String QUEUE_LMS_LOG = "lmsLog";
    public static final String CHARSET_NAME_UTF_8 = "UTF-8";
}
