package io.github.wx.common.constant;

/**
 * Created by jie on 2017/4/8.
 * 功能状态枚举
 */
public enum EnumFuctionStatus {
    NORMAL_STATUS("0", "正常"),
    DEL_STATUS("1", "删除"),
    BAD_STATUS("9", "不可用");

    private String code;
    private String msg;

    EnumFuctionStatus(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
