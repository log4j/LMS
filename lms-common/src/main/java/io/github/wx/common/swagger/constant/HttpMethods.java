package io.github.wx.common.swagger.constant;


public interface HttpMethods {
    String GET = "get";
    String POST = "post";
    String PUT = "put";
    String DELETE = "delete";
}
