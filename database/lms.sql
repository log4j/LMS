/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50162
Source Host           : localhost:3306
Source Database       : lms

Target Server Type    : MYSQL
Target Server Version : 50162
File Encoding         : 65001

Date: 2017-07-12 21:22:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `dict_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `dict_name` varchar(32) DEFAULT NULL COMMENT '字典名称',
  `dict_value` varchar(32) DEFAULT NULL COMMENT '字典值',
  `dict_type` int(11) DEFAULT NULL COMMENT '类型',
  `sort_num` int(11) DEFAULT NULL COMMENT '排序值',
  `del_flag` char(0) DEFAULT NULL COMMENT '删除标记',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`dict_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dictitem
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictitem`;
CREATE TABLE `sys_dictitem` (
  `dictitem_id` int(11) NOT NULL COMMENT 'ID',
  `dict_id` int(11) NOT NULL COMMENT '字典值',
  `dictitem_name` varchar(32) DEFAULT NULL COMMENT '字典项名称',
  `dictitem_value` varchar(32) DEFAULT NULL COMMENT '字典项值',
  `sort_num` int(11) DEFAULT NULL COMMENT '排序值',
  `del_flag` char(1) DEFAULT NULL COMMENT '删除标记',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`dictitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典项表';

-- ----------------------------
-- Records of sys_dictitem
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(32) NOT NULL COMMENT '菜单名称',
  `menu_desc` varchar(32) DEFAULT NULL COMMENT '菜单描述',
  `url` varchar(64) DEFAULT NULL,
  `permission` varchar(32) DEFAULT NULL COMMENT '权限标志',
  `parent_id` int(11) DEFAULT NULL COMMENT '父菜单ID',
  `sort` int(11) DEFAULT NULL COMMENT '排序值',
  `opened` char(5) DEFAULT '0' COMMENT '0关闭，1打开',
  `status` char(1) DEFAULT '0' COMMENT '0正常1删除9不可用',
  `type` char(1) DEFAULT NULL COMMENT '菜单类型 （0菜单 1按钮）',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` char(1) DEFAULT '0' COMMENT '0--正常 1--删除',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '权限管理', null, '', 'auth:manage', null, '0', '0', '0', '0', '2017-04-08 02:20:55', null, '0');
INSERT INTO `sys_menu` VALUES ('2', '用户管理', null, '/admin/user/userManage', 'user:manage', '1', '1', '0', '0', '0', '2017-04-08 02:21:23', null, '0');
INSERT INTO `sys_menu` VALUES ('3', '菜单管理', null, '/admin/roleMenu/menuManage', 'menu:manage', '1', '2', '0', '0', '0', '2017-04-08 02:22:02', null, '0');
INSERT INTO `sys_menu` VALUES ('4', '角色管理', null, '/admin/roleMenu/roleManage', 'role:manage', '1', '3', '0', '0', '0', '2017-04-08 02:22:12', null, '0');
INSERT INTO `sys_menu` VALUES ('58', '添加用户', null, null, 'user:add', '2', '0', '0', '0', '1', '2017-05-07 14:49:55', null, '0');
INSERT INTO `sys_menu` VALUES ('59', '修改用户', null, null, 'user:edit', '2', '1', '0', '0', '1', '2017-05-07 14:50:24', null, '0');
INSERT INTO `sys_menu` VALUES ('60', '删除用户', null, null, 'user:del', '2', '2', '0', '0', '1', '2017-05-07 14:50:48', null, '0');
INSERT INTO `sys_menu` VALUES ('61', '锁定用户', null, null, 'user:lock', '2', '3', '0', '0', '1', '2017-05-07 14:51:09', null, '0');
INSERT INTO `sys_menu` VALUES ('62', '增加角色', null, null, 'role:add', '4', '0', '0', '0', '1', '2017-05-07 15:42:04', null, '0');
INSERT INTO `sys_menu` VALUES ('63', '删除角色', null, null, 'role:del', '4', '1', '0', '0', '1', '2017-05-07 15:42:20', null, '0');
INSERT INTO `sys_menu` VALUES ('64', '修改角色', null, null, 'role:edit', '4', '2', '0', '0', '1', '2017-05-07 15:44:57', null, '0');
INSERT INTO `sys_menu` VALUES ('65', '分配权限', null, null, 'role:grantMenu', '4', '3', '0', '0', '1', '2017-05-07 15:47:18', null, '0');
INSERT INTO `sys_menu` VALUES ('66', '增加菜单', null, null, 'menu:add', '3', '0', '0', '0', '1', '2017-05-07 15:50:54', null, '0');
INSERT INTO `sys_menu` VALUES ('67', '删除菜单', null, null, 'menu:del', '3', '1', '0', '0', '1', '2017-05-07 15:51:23', null, '0');
INSERT INTO `sys_menu` VALUES ('68', '强制下线', null, null, 'user:offUser', '2', '4', '0', '0', '1', '2017-05-07 16:09:28', null, '0');
INSERT INTO `sys_menu` VALUES ('69', '系统管理', null, '', 'sys:manage', null, '0', '0', '0', '0', '2017-05-13 20:03:53', null, '0');
INSERT INTO `sys_menu` VALUES ('70', 'druid日志', null, '/druid', 'druid:manage', '69', '0', '1', '0', '0', '2017-05-13 20:14:51', null, '0');
INSERT INTO `sys_menu` VALUES ('71', 'swagger', null, '/swagger', 'swagger', '69', '0', '1', '0', '0', '2017-07-12 21:21:19', null, '0');

-- ----------------------------
-- Table structure for sys_param
-- ----------------------------
DROP TABLE IF EXISTS `sys_param`;
CREATE TABLE `sys_param` (
  `param_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `param_code` varchar(32) NOT NULL COMMENT '参数代码',
  `param_value` varchar(32) NOT NULL COMMENT '参数值',
  `param_type` varchar(10) DEFAULT NULL COMMENT '参数类型，枚举依据具体应用',
  `param_desc` varchar(32) DEFAULT NULL COMMENT '参数描述',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` char(0) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`param_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据参数表';

-- ----------------------------
-- Records of sys_param
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(32) NOT NULL COMMENT '角色名称',
  `role_desc` varchar(32) DEFAULT NULL COMMENT '角色描述',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间（默认当前时间）',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` char(1) DEFAULT '0' COMMENT '0--正常 1--删除',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', 'admin', '管理员', '2017-04-08 01:38:28', null, '0');
INSERT INTO `sys_role` VALUES ('2', 'user', '普通用户', '2017-04-08 01:38:47', null, '0');
INSERT INTO `sys_role` VALUES ('3', 'test', '测试用户', '2017-04-08 01:38:58', null, '0');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8 COMMENT='角色菜单表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('198', '1', '1');
INSERT INTO `sys_role_menu` VALUES ('199', '1', '2');
INSERT INTO `sys_role_menu` VALUES ('200', '1', '58');
INSERT INTO `sys_role_menu` VALUES ('201', '1', '59');
INSERT INTO `sys_role_menu` VALUES ('202', '1', '60');
INSERT INTO `sys_role_menu` VALUES ('203', '1', '61');
INSERT INTO `sys_role_menu` VALUES ('204', '1', '68');
INSERT INTO `sys_role_menu` VALUES ('205', '1', '3');
INSERT INTO `sys_role_menu` VALUES ('206', '1', '66');
INSERT INTO `sys_role_menu` VALUES ('207', '1', '67');
INSERT INTO `sys_role_menu` VALUES ('208', '1', '4');
INSERT INTO `sys_role_menu` VALUES ('209', '1', '62');
INSERT INTO `sys_role_menu` VALUES ('210', '1', '63');
INSERT INTO `sys_role_menu` VALUES ('211', '1', '64');
INSERT INTO `sys_role_menu` VALUES ('212', '1', '65');
INSERT INTO `sys_role_menu` VALUES ('213', '1', '69');
INSERT INTO `sys_role_menu` VALUES ('214', '1', '70');
INSERT INTO `sys_role_menu` VALUES ('215', '1', '71');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` varchar(32) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `salt` varchar(32) DEFAULT NULL COMMENT '加密盐',
  `user_desc` varchar(32) DEFAULT NULL COMMENT '用户描述',
  `img` varchar(64) DEFAULT NULL COMMENT '头像',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间（默认当前时间）',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` char(1) DEFAULT '0' COMMENT '0--正常 1--删除',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'c22aeb78445a21e309dd1686c164f5db', '3950e80f0a3ca1e665d751b52e4ab339', 'admin', null, '2017-04-07 18:51:39', '2017-05-07 14:33:53', '0');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='用户角色表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('14', '1', '1');
INSERT INTO `sys_user_role` VALUES ('27', '24', '3');
INSERT INTO `sys_user_role` VALUES ('28', '25', '3');
