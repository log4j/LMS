package io.github.wx.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.xiaoleilu.hutool.util.CollectionUtil;
import io.github.wx.common.constant.EnumFuctionStatus;
import io.github.wx.common.easyui.TreeNode;
import io.github.wx.dao.model.SysMenu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jie on 2017/4/8.
 * 菜单管理
 */
public class MenuService {
    /**
     * 获取全部的菜单并按照树结构展示
     *
     * @return List<TreeNode>
     */
    public List<TreeNode> selectAll() {
        List<TreeNode> treeNodeList = new ArrayList<>();
        // 查询所有菜单
        SqlPara sqlPara = SysMenu.dao.getSqlPara("menu.findAllMenu", EnumFuctionStatus.NORMAL_STATUS.getCode());
        List<SysMenu> menuList = SysMenu.dao.find(sqlPara);
        if (CollectionUtil.isEmpty(menuList)) {
            return null;
        }
        convertTreeNode(treeNodeList, menuList);
        return treeNodeList;
    }

    /**
     * 根绝角色ID 获取菜单列表
     *
     * @param roleId 角色ID
     * @return SysMenuList
     */
    public List<SysMenu> findByRoleId(Integer roleId) {
        SqlPara sqlPara = SysMenu.dao.getSqlPara("menu.findByRoleId", roleId, EnumFuctionStatus.NORMAL_STATUS.getCode());
        return SysMenu.dao.find(sqlPara);
    }

    /**
     * 查询角色的菜单
     *
     * @param roleId 角色Id
     * @return List<TreeNode>
     */
    public List<TreeNode> findMenuListByRoleId(int roleId) {
        List<TreeNode> treeNodeList = new ArrayList<>();
        SqlPara sqlPara = SysMenu.dao.getSqlPara("menu.findByRoleId", roleId, EnumFuctionStatus.NORMAL_STATUS.getCode());
        List<SysMenu> menuList = SysMenu.dao.find(sqlPara);
        if (CollectionUtil.isEmpty(menuList)) {
            return null;
        }
        convertTreeNode(treeNodeList, menuList);
        return treeNodeList;
    }

    /**
     * 删除菜单及其子菜单
     *
     * @param menuId 菜单ID
     */
    public void delMenu(int menuId) {
        SysMenu menu = SysMenu.dao.findById(menuId);
        menu.setDelFlag(EnumFuctionStatus.DEL_STATUS.getCode());
        menu.update();
        SqlPara sqlPara = SysMenu.dao.getSqlPara("menu.delMenu", EnumFuctionStatus.DEL_STATUS.getCode(), menuId);
        Db.update(sqlPara.getSql(), sqlPara.getPara());
    }

    /**
     * 拼装按钮
     *
     * @param treeNodeList 结果
     * @param menuList     查询结果
     */
    private void convertTreeNode(List<TreeNode> treeNodeList, List<SysMenu> menuList) {
        for (SysMenu menu : menuList) {
            TreeNode node = new TreeNode();
            node.setId(menu.getMenuId());
            if (menu.getParentId() != null) node.setPid(menu.getParentId());
            node.setText(menu.getMenuName());
            Map attributes = new HashMap<>();
            attributes.put("url", menu.getUrl());
            attributes.put("openType", menu.getOpened());
            node.setAttributes(attributes);
            node.setPermission(menu.getPermission());
            node.setSort(menu.getSort());
            node.setStatus(menu.getStatus());
            node.setType(menu.getType());
            treeNodeList.add(node);
        }
    }

    /**
     * 根据用户名查询用户的菜单
     * TODO 放入到redis 中
     *
     * @param username 用户名
     * @return List
     */
    public List<TreeNode> findUseMenuListByUserName(String username) {
        List<TreeNode> treeNodeList = new ArrayList<>();
        SqlPara sqlPara = SysMenu.dao.getSqlPara("menu.findUseMenuListByUserName", username);
        List<SysMenu> menuList = SysMenu.dao.find(sqlPara);
        convertTreeNode(treeNodeList, menuList);
        return treeNodeList;
    }
}

