package io.github.wx.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.xiaoleilu.hutool.util.CollectionUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import io.github.wx.common.constant.EnumFuctionStatus;
import io.github.wx.common.easyui.DataGrid;
import io.github.wx.dao.model.SysRole;
import io.github.wx.dao.model.SysRoleMenu;
import io.github.wx.dao.model.SysUser;

import java.util.Date;
import java.util.List;

/**
 * Created by jie on 2017/4/8.
 * 角色管理
 */
public class RoleService {
    /**
     * 分页查询角色信息
     *
     * @param page page
     * @param rows page
     * @return DataGrid
     */
    public DataGrid<SysRole> getRolePage(Integer page, Integer rows) {
        SqlPara sqlPara = SysRole.dao.getSqlPara("role.findRolePage", EnumFuctionStatus.NORMAL_STATUS.getCode());
        Page<SysRole> userPage = SysRole.dao.paginate(page, rows, sqlPara);
        return new DataGrid<>(userPage);
    }

    /**
     * 获取用户的角色
     *
     * @param loginName 登录名
     * @return SysRole
     */
    public SysRole findUserRole(String loginName) {
        SqlPara sqlPara = SysUser.dao.getSqlPara("role.findUserRole", loginName, EnumFuctionStatus.NORMAL_STATUS.getCode());
        return SysRole.dao.findFirst(sqlPara);
    }

    /**
     * 获取角色的id-value对
     *
     * @return List
     */
    public List<SysRole> roleCombo() {
        SqlPara sqlPara = SysUser.dao.getSqlPara("role.findRoleCombo", EnumFuctionStatus.NORMAL_STATUS.getCode());
        return SysRole.dao.find(sqlPara);
    }

    /**
     * 角色分配菜单
     *
     * @param roleId  1
     * @param menuIds 1,2，3,4,5
     */
    public void grantMenu(int roleId, String menuIds) {
        if (StrUtil.isNotEmpty(menuIds)) {
            String menuArray[] = menuIds.split(",");
            for (String menuId : menuArray) {
                SysRoleMenu roleMenu = new SysRoleMenu();
                roleMenu.setRoleId(roleId);
                roleMenu.setMenuId(Integer.parseInt(menuId));
                roleMenu.save();
            }
        }
    }

    /**
     * 删除用户权限
     *
     * @param roleId 角色ID
     */
    public void removeRoleMenu(int roleId) {
        SqlPara sqlPara = SysRoleMenu.dao.getSqlPara("role.delRoleMenu", roleId);
        Db.update(sqlPara.getSql(), sqlPara.getPara());
    }

    /**
     * 根据角色名称查询角色是否存在
     *
     * @param roleName 角色名称
     * @return BOLLEAN
     */
    public boolean findRoleByName(String roleName) {
        SqlPara sqlPara = SysRole.dao.getSqlPara("role.findRoleByName", roleName, EnumFuctionStatus.NORMAL_STATUS.getCode());
        List<SysRole> roleList = SysRole.dao.find(sqlPara);
        return CollectionUtil.isNotEmpty(roleList);
    }

    /**
     * 删除角色
     *
     * @param roleId 角色ID
     */
    public void deleteRoleById(int roleId) {
        SysRole sysRole = SysRole.dao.findById(roleId);
        sysRole.setDelFlag(EnumFuctionStatus.DEL_STATUS.getCode());
        sysRole.setUpdateTime(new Date());
        sysRole.update();
    }

    /**
     * 根据角色ID查询角色
     *
     * @param roleId 角色ID
     * @return SYSROLE
     */
    public SysRole findRoleById(int roleId) {
        return SysRole.dao.findById(roleId);
    }
}
