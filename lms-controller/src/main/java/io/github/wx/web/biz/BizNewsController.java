package io.github.wx.web.biz;

import com.jfinal.aop.Duang;
import io.github.wx.common.controller.BaseController;
import io.github.wx.common.easyui.DataGrid;
import io.github.wx.service.BizNewsService;

/**
 * Created by jie on 2017/4/21.
 * BizNewsController
 */
public class BizNewsController extends BaseController {
    static final BizNewsService bizNewsService = Duang.duang(BizNewsService.class);

    /**
     * 新闻管理
     */
    public void newsManage() {
        render("newsManage.html");
    }

    /**
     * 新闻列表
     */
    public void newsList() {
        DataGrid dataGrid = bizNewsService.getNewsPage(getParaToInt("page"), getParaToInt("rows"));
        renderJson(dataGrid);
    }
}
