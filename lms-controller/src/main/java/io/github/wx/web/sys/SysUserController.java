package io.github.wx.web.sys;

import com.jfinal.aop.Duang;
import com.jfinal.upload.UploadFile;
import com.xiaoleilu.hutool.util.CollectionUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import io.github.wx.common.controller.BaseController;
import io.github.wx.common.easyui.DataGrid;
import io.github.wx.common.shiro.kit.ShiroSessionKit;
import io.github.wx.common.shiro.kit.UserSessionVo;
import io.github.wx.common.swagger.annotation.Api;
import io.github.wx.common.swagger.annotation.ApiOperation;
import io.github.wx.common.swagger.annotation.Param;
import io.github.wx.common.swagger.annotation.Params;
import io.github.wx.common.swagger.constant.DataType;
import io.github.wx.common.swagger.constant.HttpMethods;
import io.github.wx.common.util.FileUploadKit;
import io.github.wx.dao.model.SysUser;
import io.github.wx.service.UserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import java.io.IOException;
import java.util.List;

/**
 * Created by jie on 2017/4/10.
 * 用户管理
 */
@Api(tag = "user", description = "用户管理")
public class SysUserController extends BaseController {
    static final UserService userService = Duang.duang(UserService.class);

    /**
     * 用户管理
     */
    public void userManage() {
        render("userManage.html");
    }

    /**
     * 用户新增页面
     */
    public void userAddPage() {
        render("userAdd.html");
    }

    /**
     * Z
     * 用户新增
     */
    public void userAdd() throws IOException {
        UploadFile uploadFile = getFile();
        SysUser sysUser = getBean(SysUser.class, "", true);
        List<SysUser> userList = userService.getUserByUsername(sysUser);
        if (CollectionUtil.isEmpty(userList)) {
            String fileName = FileUploadKit.mv(uploadFile, getUsername());
            if (StrUtil.isNotEmpty(fileName)) {
                sysUser.setImg(fileName);
            }
            int roleId = getParaToInt("roleId");
            userService.addUser(sysUser, roleId);
            renderSuccess("添加成功");
        } else {
            renderFailed("用户名已存在");
        }
    }

    /**
     * 编辑用户页面
     */
    public void userEditPage() {
        int userId = getParaToInt(0);
        SysUser sysUser = userService.getUserById(userId);
        setAttr("user", sysUser);
        render("userEdit.html");
    }

    /**
     * 编辑用户
     *
     * @throws IOException IOException
     */
    @RequiresPermissions("user:edit")
    public void userEdit() throws IOException {
        UploadFile uploadFile = getFile();
        SysUser sysUser = getBean(SysUser.class, "", true);
        String oldPwd = getPara("oldpassword");
        int oldRoleId = getParaToInt("oldRoleId");
        String fileName = FileUploadKit.mv(uploadFile, getUsername());
        if (StrUtil.isNotEmpty(fileName)) {
            sysUser.setImg(fileName);
        }
        userService.updateUser(sysUser, oldPwd, oldRoleId);
        renderSuccess("修改成功");
    }

    /**
     * 删除用户
     */
    @ApiOperation(url = "/admin/user/userDel", tag = "user", httpMethod = HttpMethods.GET, description = "用户删除")
    @Params({@Param(name = "id", required = true, dataType = DataType.INTEGER, description = "用户ID")})
    @RequiresPermissions("user:del")
    public void userDel() {
        int userId = getParaToInt("id");
        userService.deleteUserById(userId);
        renderSuccess("删除成功");
    }

    /**
     * 锁定用户
     */
    @RequiresPermissions("user:lock")
    public void userLock() {
        int userId = getParaToInt(0);
        userService.lockUserById(userId);
        renderSuccess("锁定成功");
    }

    /**
     * 获取用户列表
     */
    public void userList() {
        DataGrid dataGrid = userService.getUserPage(getParaToInt("page"), getParaToInt("rows"));
        renderJson(dataGrid);
    }

    /**
     * 当前在线用户列表页
     */
    public void onlineUserPage() {
        List<UserSessionVo> userSessionVoList = ShiroSessionKit.getAllSessionRelation();
        setAttr("userList", userSessionVoList);
        render("userOnline.html");
    }

    @RequiresPermissions("user:offUser")
    public void offUser() {
        String username = getPara(0);
        if (!getUsername().equals(username)) {
            ShiroSessionKit.delSession(username);
            renderSuccess("下线成功");
        } else {
            renderFailed("不允许下线自己");
        }

    }
}
