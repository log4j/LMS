package io.github.wx.web.sys;

import com.jfinal.aop.Before;
import com.jfinal.aop.Duang;
import com.jfinal.plugin.redis.Redis;
import com.xiaoleilu.hutool.util.CollectionUtil;
import io.github.wx.common.controller.BaseController;
import io.github.wx.common.easyui.DataGrid;
import io.github.wx.common.easyui.TreeNode;
import io.github.wx.common.redis.RedisCacheInterceptor;
import io.github.wx.common.shiro.cache.RedisCache;
import io.github.wx.common.swagger.annotation.Api;
import io.github.wx.common.swagger.annotation.ApiOperation;
import io.github.wx.common.swagger.annotation.Param;
import io.github.wx.common.swagger.annotation.Params;
import io.github.wx.common.swagger.constant.DataType;
import io.github.wx.common.swagger.constant.HttpMethods;
import io.github.wx.dao.model.SysMenu;
import io.github.wx.dao.model.SysRole;
import io.github.wx.dao.model.SysUser;
import io.github.wx.service.MenuService;
import io.github.wx.service.RoleService;
import io.github.wx.service.UserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import java.util.Date;
import java.util.List;

/**
 * Created by jie on 2017/4/7.
 * 后台基本管理
 * /admin
 */
@Api(tag = "roleMenu", description = "角色菜单管理")
public class SysRoleMenuController extends BaseController {
    static final RoleService roleService = Duang.duang(RoleService.class);
    static final MenuService menuService = Duang.duang(MenuService.class);

    /**
     * 角色管理
     */
    public void roleManage() {
        render("role/roleManage.html");
    }

    /**
     * 添加角色页面
     */
    public void roleAddPage() {
        render("role/roleAdd.html");
    }

    /**
     * 添加角色
     */
    public void roleAdd() {
        SysRole sysRole = getBean(SysRole.class, "", true);
        String roleName = sysRole.getRoleName();
        boolean isExist = roleService.findRoleByName(roleName);
        if (!isExist) {
            sysRole.save();
            renderSuccess("操作成功");
        } else {
            renderFailed("角色名称已存在");
        }
    }

    /**
     * 编辑角色页面
     */
    public void roleEditPage() {
        int roleId = getParaToInt(0);
        SysRole sysRole = roleService.findRoleById(roleId);
        setAttr("role", sysRole);
        render("role/roleEdit.html");
    }

    /**
     * 编辑角色
     */
    public void roleEdit() {
        SysRole sysRole = getBean(SysRole.class, "", true);
        sysRole.setUpdateTime(new Date());
        sysRole.update();
        renderSuccess("操作成功");
    }

    /**
     * 删除角色
     */
    @ApiOperation(url = "/admin/roleMenu/roleDel", tag = "roleMenu", httpMethod = HttpMethods.GET, description = "角色删除")
    @Params({@Param(name = "id", required = true, dataType = DataType.INTEGER, description = "角色ID")})
    public void roleDel() {
        int roleId = getParaToInt(0);
        roleService.deleteRoleById(roleId);
        renderSuccess("删除成功");
    }

    /**
     * 用户主页菜单
     */
    public void menuTree() {
        List<TreeNode> menuList = menuService.findUseMenuListByUserName(getUsername());
        renderJson(menuList);
    }

    /**
     * 获取角色列表
     */
    public void roleList() {
        DataGrid<SysRole> dataGrid = roleService.getRolePage(getParaToInt("page"), getParaToInt("rows"));
        renderJson(dataGrid);
    }

    /**
     * 获取角色id-value
     */
    @Before(RedisCacheInterceptor.class)
    public void roleCombo() {
        List<SysRole> roleList = roleService.roleCombo();
        renderJson(roleList);
    }

    /**
     * 菜单管理
     */
    public void menuManage() {
        render("menu/menuManage.html");
    }

    /**
     * 获取菜单列表
     */
    public void menuList() {
        List<TreeNode> treeNodeList = menuService.selectAll();
        renderJson(treeNodeList);
    }

    /**
     * 添加菜单页面
     */
    public void menuAddPage() {
        render("menu/menuAdd.html");
    }

    /**
     * 添加菜单
     */
    @RequiresPermissions("menu:add")
    public void addMenu() {
        SysMenu sysMenu = getBean(SysMenu.class, "", true);
        if (sysMenu.save()) {
            renderSuccess("添加成功");
        } else {
            renderFailed("添加失败");
        }
    }

    /**
     * 分配菜单页面
     */
    public void grantPage() {
        int roleId = getParaToInt(0);
        setAttr("roleId", roleId);
        render("role/roleGrant.html");
    }

    /**
     * 根据用户ID 查询菜单
     */
    public void findMenuListByRoleId() {
        int roleId = getParaToInt("roleId");
        List<TreeNode> treeNodeList = menuService.findMenuListByRoleId(roleId);
        renderJson(treeNodeList);
    }

    /**
     * 角色分配菜单
     */
    @RequiresPermissions("role:grantMenu")
    public void grantMenu() {
        int roleId = getParaToInt("roleId");
        String menuIds = getPara("menuIds");
        //先删除角色原有的菜单
        roleService.removeRoleMenu(roleId);
        //分配权限
        roleService.grantMenu(roleId, menuIds);
        //更新相关用户的授权信息
        UserService userService = Duang.duang(UserService.class);
        List<SysUser> userList = userService.getUserByRoleId(roleId);
        if (CollectionUtil.isNotEmpty(userList)) {
            for (SysUser sysUser : userList) {
                Redis.use().del(RedisCache.SHIRO_KEY + sysUser.getUsername());
            }
        }
        renderSuccess("分配成功");
    }

    /**
     * 删除菜单及其子菜单
     */
    @RequiresPermissions("menu:del")
    public void delMenu() {
        int menuId = getParaToInt("id");
        menuService.delMenu(menuId);
        renderSuccess("操作成功");
    }

}
