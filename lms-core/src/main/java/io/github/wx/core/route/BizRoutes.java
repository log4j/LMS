package io.github.wx.core.route;

import com.jfinal.config.Routes;
import io.github.wx.web.biz.BizNewsController;

/**
 * Created by jie on 2017/4/21.
 * 业务模块路由
 */
public class BizRoutes extends Routes {
    public void config() {
        setBaseViewPath("/WEB-INF/views");
        add("/admin/news", BizNewsController.class, "/admin/biz");
    }
}
