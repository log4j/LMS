package io.github.wx.dao.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseBizNews<M extends BaseBizNews<M>> extends Model<M> implements IBean {

	public void setId(Integer id) {
		set("id", id);
	}

	public Integer getId() {
		return get("id");
	}

	public void setSiteName(String siteName) {
		set("site_name", siteName);
	}

	public String getSiteName() {
		return get("site_name");
	}

	public void setAuthorName(String authorName) {
		set("author_name", authorName);
	}

	public String getAuthorName() {
		return get("author_name");
	}

	public void setUrl(String url) {
		set("url", url);
	}

	public String getUrl() {
		return get("url");
	}

	public void setTitle(String title) {
		set("title", title);
	}

	public String getTitle() {
		return get("title");
	}

	public void setSummary(String summary) {
		set("summary", summary);
	}

	public String getSummary() {
		return get("summary");
	}

	public void setPublishDate(String publishDate) {
		set("publish_date", publishDate);
	}

	public String getPublishDate() {
		return get("publish_date");
	}

	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}

	public java.util.Date getCreateTime() {
		return get("create_time");
	}

}
