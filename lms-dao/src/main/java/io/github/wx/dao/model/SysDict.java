package io.github.wx.dao.model;

import io.github.wx.dao.model.base.BaseSysDict;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class SysDict extends BaseSysDict<SysDict> {
	public static final SysDict dao = new SysDict().dao();
}
