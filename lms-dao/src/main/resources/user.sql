#namespace("user")
#sql("findUserByName")
SELECT
  sys_user.user_id,
  sys_user.username,
  sys_user.`password`,
  sys_user.salt,
  sys_user.create_time,
  sys_user.update_time,
  sys_user.del_flag
FROM sys_user
WHERE username = #para(0) AND del_flag <> #para(1)
#end

#sql("findUserById")
SELECT
  `user`.user_id,
  `user`.username,
  `user`.`password`,
  `user`.salt,
  `user`.user_desc,
  `user`.img,
  `user`.create_time,
  `user`.update_time,
  `user`.del_flag,
  role.role_id,
  role.role_name
FROM
  sys_user AS `user`
  INNER JOIN sys_user_role AS ur ON `user`.user_id = ur.user_id
  INNER JOIN sys_role AS role ON ur.role_id = role.role_id
WHERE `user`.user_id = #para(0) AND `user`.del_flag <> #para(1)
#end

#sql("findUserRole")
SELECT
  ur.id,
  ur.user_id,
  ur.role_id
FROM
  sys_user_role AS ur
WHERE
  ur.user_id = #para(0)
#end

#sql("findUserPage")
SELECT
  `user`.user_id,
  `user`.username,
  `user`.`password`,
  `user`.salt,
  `user`.user_desc,
  `user`.img,
  `user`.create_time,
  `user`.update_time,
  `user`.del_flag,
  role.role_name
FROM
  sys_user AS `user`
  INNER JOIN sys_user_role AS ur ON `user`.user_id = ur.user_id
  INNER JOIN sys_role AS role ON ur.role_id = role.role_id
WHERE
  USER.del_flag <> #para(0)
  AND role.del_flag = #para(1)
#end

#sql("findyUserByRoleId")
SELECT
  u.user_id,
  u.username,
  u.`password`,
  u.salt,
  u.user_desc,
  u.img,
  u.create_time,
  u.update_time,
  u.del_flag,
  ur.role_id
FROM
  sys_user u
INNER JOIN sys_user_role ur ON ur.user_id = u.user_id
AND ur.role_id = #para(0)
#end

#sql("findUsers")
  SELECT * FROM sys_user u WHERE u.del_flag = #para(flag) and u.user_id in (#para(ids))
#end

#end