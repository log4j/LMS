#namespace("role")
#sql("findUserRole")
SELECT
  role.role_id,
  role.role_name,
  role.role_desc,
  role.create_time,
  role.update_time,
  role.del_flag
FROM
  sys_role AS role
  INNER JOIN sys_user_role AS ur ON role.role_id = ur.role_id
  INNER JOIN sys_user AS `user` ON ur.user_id = `user`.user_id
WHERE
  `user`.username = #para(0) AND
  role.del_flag = #para(1)
#end

#sql("findRoleCombo")
SELECT
  role.role_id,
  role.role_name
FROM
  sys_role AS role
WHERE
  role.del_flag = #para(0)
#end

#sql("findRolePage")
SELECT *
FROM
  sys_role
WHERE
  del_flag = #para(0)
#end

#sql("delRoleMenu")
DELETE FROM sys_role_menu WHERE role_id = #para(0)
#end

#sql("findRoleByName")
SELECT *
FROM
  sys_role
WHERE
  role_name = #para(0)
  AND del_flag = #para(1)
#end
#end