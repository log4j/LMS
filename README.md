# LMS  
#### Library Manager System

#### 图书管理系统
  
 - 系统框架采用 jfinal 3.0 完成
 - 权限、会话管理采用 shiro 1.2.4 完成
 - 缓存部分采用redis
 
#### 启动说明
本项目依赖的shiro通用部分已抽取成单独的项目
http://git.oschina.net/log4j/jfinalshiroplugin  

- IDEA 启动设置
  JFinal.start("lms-web/src/main/webapp", 80, "/");
- eclipse 启动设置 (/开头)
 JFinal.start("/C:/workspace/LMS/lms-web/src/main/webapp/lms-web/src/main/webapp", 80, "/",10);
 
- 初始密码 
    admin  admin1

#### 参考
@[spring-shiro-training](http://git.oschina.net/wangzhixuan/spring-shiro-training) 
基于Maven构建的springmvc、spring、mybatis-plus、shiro、log4j2、easyui简单实用的权限系统。

#### 扩展
- 扩展jfinal engine支持 shiro 标签
- 扩展shiro cache ，支持redis 缓存 
- 扩展CacheInterceptor，支持redis缓存 renderInfo
- 自定义ScanJarStringSource，支持Jar包sql扫描（暂不支持热修改sql，会出现重复reload）
#### 感谢

- jfinal     @[jfinal](http://git.oschina.net/jfinal/)    
- easyui-super-theme @[在雨中](http://git.oschina.net/longyu)

#### 建议反馈 
[![如梦技术](http://pub.idqqimg.com/wpa/images/group.png)](http://shang.qq.com/wpa/qunwpa?idkey=04c31509b25bae7cc43cc369bbe432cfbfc1009f94bf879124665634c5710b8b)
<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=04c31509b25bae7cc43cc369bbe432cfbfc1009f94bf879124665634c5710b8b"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="JFinal2.2视频-如梦技术" title="JFinal2.2视频-如梦技术"></a> 

####部分截图
![image](http://obq1lvsd9.bkt.clouddn.com/1.png)  
![image](http://obq1lvsd9.bkt.clouddn.com/role.png)  
![image](http://obq1lvsd9.bkt.clouddn.com/menumange.png )  
![image](http://obq1lvsd9.bkt.clouddn.com/userList.png )  
![image](http://obq1lvsd9.bkt.clouddn.com/rabbit.png)  
![image](http://obq1lvsd9.bkt.clouddn.com/lmsswagger.png)  
![image](http://obq1lvsd9.bkt.clouddn.com/QQ%E6%88%AA%E5%9B%BE20170408033815.png)  



  
  